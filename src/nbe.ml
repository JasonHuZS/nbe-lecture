exception Error


(* Definition of syntax *)

type typ =
  | Nat
  | Func of typ * typ

type ctx = typ list

type ast =
  | Var   of int
  | Zero
  | Succ  of ast
  | Rec   of typ * ast * ast * ast
  | Lam   of ast
  | App   of ast * ast
  | Subst of ast * subst
  (* explicit substitutions *)
and subst =
  | Wk
  | Id
  | Comp  of subst * subst
  | Ext   of subst * ast

(* neutral forms *)
type ne =
  | NVar  of int
  | NRec  of typ * nf * nf * ne
  | NApp  of ne * nf
  (* normal forms *)
and nf =
  | Ne    of ne
  | NZero
  | NSucc of nf
  | NLam  of nf

let rec ne_emb (e : ne) =
  match e with
  | NVar x              -> Var x
  | NRec (ty, w, w', e) -> Rec (ty, nf_emb w, nf_emb w', ne_emb e)
  | NApp (e, w)         -> App (ne_emb e, nf_emb w)
and nf_emb w =
  match w with
  | Ne e                -> ne_emb e
  | NZero               -> Zero
  | NSucc w             -> Succ (nf_emb w)
  | NLam w              -> Lam (nf_emb w)


(* Definition of domain *)


(* domain values *)
type domain =
  | DZe
  | DSu  of domain
  | DLam of ast * env
  | Up   of typ * domneu
  (* domain environment *)
and env = int -> domain
(* domain neutral values *)
and domneu =
  | Loc  of int
  | DRec of typ * domnor * ast * env * domneu
  | DApp of domneu * domnor
  (* domain normal values *)
and domnor =
  | Down of typ * domain

let emp : env = fun _ -> DZe

let ext rho d n =
  if n == 0 then d
  else rho (n - 1)

let drop rho n =
  rho (1 + n)


(* Normalization by evaluation *)

let rec eval_ast (t : ast) rho =
  match t with
  | Var x                  -> rho x
  | Zero                   -> DZe
  | Succ t                 -> DSu (eval_ast t rho)
  | Rec (ty, zc, sc, n)    -> dom_rec ty (eval_ast zc rho) sc rho (eval_ast n rho)
  | Lam t                  -> DLam (t, rho)
  | App (s, t)             -> dom_app (eval_ast s rho) (eval_ast t rho)
  | Subst (t, ss)          -> eval_ast t (eval_subst ss rho)
and eval_subst (ss : subst) rho =
  match ss with
  | Wk                     -> drop rho
  | Id                     -> rho
  | Comp (ss, ss')         -> eval_subst ss (eval_subst ss' rho)
  | Ext (ss, t)            -> ext (eval_subst ss rho) (eval_ast t rho)
and dom_app f a =
  match f with
  | DLam (t, rho)          -> eval_ast t (ext rho a)
  | Up (Func (ty, ty'), e) -> Up (ty', (DApp (e, Down (ty, a))))
  | _                      -> raise Error
and dom_rec ty a t rho (n : domain) =
  match n with
  | DZe                    -> a
  | DSu n'                 -> eval_ast t (ext (ext rho n') (dom_rec ty a t rho n'))
  | Up (_, e)              -> Up (ty, DRec (ty, Down (ty, a), t, rho, e))
  | _                      -> raise Error

let rec readback_normal n (Down (ty, d)) =
  match (ty, d) with
  | (Nat, DZe)              -> NZero
  | (Nat, DSu d)            -> NSucc (readback_normal n (Down (Nat, d)))
  | (Nat, Up (_, e))        -> Ne (readback_neutral n e)
  | (Nat, _)                -> raise Error
  | (Func (ty, ty'), d)     -> NLam (readback_normal (1 + n) (Down (ty', dom_app d (Up (ty, Loc n)))))
and readback_neutral n e =
  match e with
  | Loc x                   -> NVar (n - x - 1)
  | DRec (ty, d, t, rho, e) -> NRec (ty,
                                     readback_normal n d,
                                     readback_normal (2 + n)
                                       (Down (ty, eval_ast t (ext (ext rho (Up (Nat, Loc n))) (Up (ty, Loc (1 + n)))))),
                                     readback_neutral n e)
  | DApp (e, d)             -> NApp (readback_neutral n e, readback_normal n d)

let rec init_env (g : ctx) =
  match g with
  | []      -> emp
  | ty :: g -> ext (init_env g) (Up (ty, Loc (List.length g)))

let nbe g t ty =
  readback_normal (List.length g) (Down (ty, eval_ast t (init_env g)))

let nbe_closed = nbe []

(* Utilities *)

let rec typ_to_string (t : typ) : string =
  match t with
  | Nat         -> "Nat"
  | Func (s, t) -> typ_to_string_func s ^ " -> " ^ typ_to_string t
and typ_to_string_func t =
  match t with
  | Nat         -> typ_to_string t
  | Func _      ->  "(" ^ typ_to_string t ^ ")"

let rec ast_to_string (t : ast) : string =
  match t with
  | Var x                    -> "v" ^ Int.to_string x
  | Zero                     -> "zero"
  | Succ t                   -> "succ " ^ ast_to_string_succ t
  | Rec (ty, zc, sc, n)      -> "rec " ^ ast_to_string_rec zc ^ " "
                                ^ ast_to_string_rec sc ^ " "
                                ^ ast_to_string_rec n ^ ": "
                                ^ typ_to_string ty
  | Lam t                    -> "lam : " ^ ast_to_string_lam t
  | App (t, s)               -> ast_to_string_app1 t ^ " " ^ ast_to_string_app2 s
  | Subst (t, ss)            -> ast_to_string t ^ " [ " ^ subst_to_string ss ^ " ]"
and subst_to_string ss =
  match ss with
  | Wk                       -> "wk"
  | Id                       -> "id"
  | Comp (ss, ss')           -> subst_to_string_comp ss ^ " ∘ " ^ subst_to_string ss'
  | Ext (ss, t)              -> subst_to_string ss ^ " , " ^ ast_to_string t
and ast_to_string_succ t =
  match t with
  | Succ _ | Rec _
  | App _ | Subst _          -> "(" ^ ast_to_string t ^ ")"
  | _                        -> ast_to_string t
and ast_to_string_rec t =
  match t with
  | Succ _ | Rec _ | App _
  | Lam _ | Subst _          -> "(" ^ ast_to_string t ^ ")"
  | _                        -> ast_to_string t
and ast_to_string_lam t =
  match t with
  | Subst _                  -> "(" ^ ast_to_string t ^ ")"
  | _                        -> ast_to_string t
and ast_to_string_app1 t =
  match t with
  | Succ _ | Rec _
  | Lam _ | Subst _          -> "(" ^ ast_to_string t ^ ")"
  | _                        -> ast_to_string t
and ast_to_string_app2 t =
  match t with
  | Succ _ | Rec _ | App _
  | Lam _ | Subst _          -> "(" ^ ast_to_string t ^ ")"
  | _                        -> ast_to_string t
and subst_to_string_comp ss =
  match ss with
  | Comp (_, _) | Ext (_, _) -> "(" ^ subst_to_string ss ^ ")"
  | _                        -> subst_to_string ss

let nf_to_string w = ast_to_string (nf_emb w)

let rec int_to_ast n =
  if n == 0 then Zero
  else Succ (int_to_ast (n - 1))


(* Definitions *)

let plus =
  Lam (Lam (Rec (Nat, Var 0, Succ (Var 0), Var 1)))


let run_prog prog ty =
  print_endline ("Program is: " ^ ast_to_string prog);
  print_endline ("Normalizes to: " ^ nf_to_string (nbe_closed prog ty));
  print_endline "--------";
  print_newline ()

let () =
  run_prog (App (App (plus, (int_to_ast 2)), (int_to_ast 3))) Nat;
  run_prog (App (plus, int_to_ast 0)) (Func (Nat, Nat));
  run_prog (App (plus, int_to_ast 1)) (Func (Nat, Nat));
  ()
