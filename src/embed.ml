exception Error

type typ =
  | Nat
  | Func of typ * typ

type ast =
  | Var  of int
  | Zero
  | Succ of ast
  | Rec  of typ * ast * ast * ast
  | Lam  of typ * ast
  | App  of ast * ast

type value =
  | NatV  of int
  | FuncV of (value -> value)


type env = value list

let rec embed (g : env) (trm : ast) : value =
  match trm with
  | Var x        ->
    begin match List.nth_opt g x with
      | None     -> raise Error
      | Some v   -> v
    end
  | Zero         -> NatV 0
  | Succ t       ->
    begin match embed g t with
      | NatV n   -> NatV (1 + n)
      | FuncV _  -> raise Error
    end
  | Rec (_, zc, sc, n)
    ->
    begin match embed g n with
      | NatV n   -> embed_rec g zc sc n
      | FuncV _  -> raise Error
    end
  | Lam (_, t)   ->
    FuncV (fun x -> embed (x :: g) t)
  | App (t, s)   ->
    begin match embed g t with
      | NatV _   -> raise Error
      | FuncV f  -> f (embed g s)
    end

and embed_rec g zc sc n : value =
  if n == 0 then embed g zc
  else let n' = n - 1
    in embed (embed_rec g zc sc n' :: NatV n' :: g) sc


let embed_closed = embed []


(* Utilities *)

let rec typ_to_string (t : typ) : string =
  match t with
  | Nat         -> "Nat"
  | Func (s, t) -> typ_to_string_func s ^ " -> " ^ typ_to_string t
and typ_to_string_func t =
  match t with
  | Nat         -> typ_to_string t
  | Func _      ->  "(" ^ typ_to_string t ^ ")"

let rec ast_to_string (t : ast) : string =
  match t with
  | Var x                  -> "v" ^ Int.to_string x
  | Zero                   -> "zero"
  | Succ t                 -> "succ " ^ ast_to_string_succ t
  | Rec (ty, zc, sc, n)    -> "rec " ^ ast_to_string_rec zc ^ " "
                              ^ ast_to_string_rec sc ^ " "
                              ^ ast_to_string_rec n ^ ": "
                              ^ typ_to_string ty
  | Lam (ty, t)            -> "lam " ^ typ_to_string ty ^ ": " ^ ast_to_string t
  | App (t, s)             -> ast_to_string_app1 t ^ " " ^ ast_to_string_app2 s
and ast_to_string_succ t =
  match t with
  | Succ _ | Rec _ | App _ -> "(" ^ ast_to_string t ^ ")"
  | _                      -> ast_to_string t
and ast_to_string_rec t =
  match t with
  | Succ _ | Rec _
  | App _ | Lam _          -> "(" ^ ast_to_string t ^ ")"
  | _                      -> ast_to_string t
and ast_to_string_app1 t =
  match t with
  | Succ _ | Rec _
  | Lam _                  -> "(" ^ ast_to_string t ^ ")"
  | _                      -> ast_to_string t
and ast_to_string_app2 t =
  match t with
  | Succ _ | Rec _
  | App _ | Lam _          -> "(" ^ ast_to_string t ^ ")"
  | _                      -> ast_to_string t

let rec int_to_ast n =
  if n == 0 then Zero
  else Succ (int_to_ast (n - 1))

let value_to_string (v : value) =
  match v with
  | NatV n  -> ast_to_string (int_to_ast n)
  | FuncV _ -> "<lambda>"


(* Definitions *)

let plus =
  Lam (Nat, Lam (Nat, Rec (Nat, Var 0, Succ (Var 0), Var 1)))

let run_prog prog =
  print_endline ("Program is: " ^ ast_to_string prog);
  print_endline ("Evaluates to: " ^ value_to_string (embed_closed prog));
  print_endline "--------";
  print_newline ()

let () =
  run_prog (App (App (plus, (int_to_ast 2)), (int_to_ast 3)));
  run_prog (App (plus, int_to_ast 0));
  ()
